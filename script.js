
const unidadesPorCategoria = {
    comprimento: ["metro", "centimetro", "quilometro"],
    massa: ["quilograma", "grama", "miligrama"],
    volume: ["litro", "mililitro", "metro cubico"],
    tempo: ["segundo", "minuto", "hora"]
};

const categoriaSelect = document.getElementById("categoria");
const unidadeOrigemSelect = document.getElementById("unidadeOrigem");
const unidadeDestinoSelect = document.getElementById("unidadeDestino");

categoriaSelect.addEventListener("change", popularUnidades);

function popularUnidades() {
    const categoria = categoriaSelect.value;
    const unidades = unidadesPorCategoria[categoria];

    unidadeOrigemSelect.innerHTML = "";
    unidadeDestinoSelect.innerHTML = "";

    unidades.forEach(unidade => {
        const optionOrigem = document.createElement("option");
        const optionDestino = document.createElement("option");
        optionOrigem.text = unidade;
        optionOrigem.value = unidade;
        optionDestino.text = unidade;
        optionDestino.value = unidade;
        unidadeOrigemSelect.add(optionOrigem);
        unidadeDestinoSelect.add(optionDestino);
    });
}

function converterUnidades() {
    const valor = parseFloat(document.getElementById("valor").value);
    const unidadeOrigem = document.getElementById("unidadeOrigem").value;
    const unidadeDestino = document.getElementById("unidadeDestino").value;
    let resultado;

    if (categoriaSelect.value === "comprimento") {
        resultado = converterComprimento(valor, unidadeOrigem, unidadeDestino);
    } else if (categoriaSelect.value === "massa") {
        resultado = converterMassa(valor, unidadeOrigem, unidadeDestino);
    } else if (categoriaSelect.value === "volume") {
        resultado = converterVolume(valor, unidadeOrigem, unidadeDestino);
    } else if (categoriaSelect.value === "tempo") {
        resultado = converterTempo(valor, unidadeOrigem, unidadeDestino);
    }

    document.getElementById("resultado").textContent = `Resultado: ${resultado}`;
}

function converterComprimento(valor, unidadeOrigem, unidadeDestino) {
    if (unidadeOrigem === "metro") {
        if (unidadeDestino === "centimetro") {
            return valor * 100;
        } else if (unidadeDestino === "quilometro") {
            return valor / 1000;
        }
    } else if (unidadeOrigem === "centimetro") {
        if (unidadeDestino === "metro") {
            return valor / 100;
        } else if (unidadeDestino === "quilometro") {
            return valor / 100000;
        }
    } else if (unidadeOrigem === "quilometro") {
        if (unidadeDestino === "metro") {
            return valor * 1000;
        } else if (unidadeDestino === "centimetro") {
            return valor * 100000;
        }
    }

    return valor;
}

function converterMassa(valor, unidadeOrigem, unidadeDestino) {
    return valor;
}

function converterVolume(valor, unidadeOrigem, unidadeDestino) {
    return valor;
}

function converterTempo(valor, unidadeOrigem, unidadeDestino) {
    return valor;
}

popularUnidades();